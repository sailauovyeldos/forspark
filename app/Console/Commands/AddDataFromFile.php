<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AddDataFromFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:add_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = storage_path() . "/json/categories.json";
        $categories = json_decode(file_get_contents($path), true);

        foreach ($categories as $category) {
            $length = strlen($category['title']);
            if ($length <= 12) {
                \App\Models\Category::create($category);
            }
        }

        $path = storage_path() . "/json/product.json";
        $products = json_decode(file_get_contents($path), true);

        foreach ($products as $product) {
            $length = strlen($product['title']);
            if ($length <= 12) {
                if ($product['price'] >= 0 and $product['price'] <= 200) {
                    $addedProduct = \App\Models\Product::create($product);
                }
            }
            foreach ($product['categories_e_id'] as $item) {
                if (\App\Models\Category::where('e_id', $item)->first()) {
                    \DB::table('category_products')->insert([
                        'category_id' => \App\Models\Category::where('e_id', $item)->first()->id,
                        'product_id' => $addedProduct->id
                    ]);
                }
            }
        }
    }
}
