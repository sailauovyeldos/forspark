<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use Illuminate\Support\Facades\Mail;

class ProductController extends Controller
{
    public function create(Request $request) {
        $rules = [
            'title' => 'required|string|min:3|max:12',
            'price' => 'required|numeric|min:0|max:200',
            'e_id' => 'nullable|integer'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response($validator->failed());
        }

        Product::create($request->all());

        $templateData = "HI";
        // Mail::send('email.notification', $templateData, 
        //     function ($m) use ($templateData) {
        //     $m->from(getenv('MAIL_FROM_ADDRESS'), getenv('MAIL_FROM_NAME'));
        //     $m->to([getenv('MAIL_TO_ADDRESS')])->subject('add data');
        // });
        return response()->json(['message' => 'the product is successfully created!'], 200);
    }

    public function all() {
        $path = storage_path() . "/json/product.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        $jsons = json_decode(file_get_contents($path), true);
        foreach ($jsons as $json) {

            foreach ($json['categories_e_id'] as $item) {
                if (\App\Models\Category::where('e_id', $item)->first()) {
                    dump(\App\Models\Category::where('e_id', $item)->first()->id);
                }
            }
        }
        dd('HI');
        return response()->json(Product::all(), 200);
    }

    public function index($id) {
        $product = Product::findOrFail($id);

        return response()->json($product, 200);
    }

    public function update(Request $request, $id) {
        $rules = [
            'title' => 'required|string|min:3|max:12',
            'price' => 'required|numeric|min:0|max:200',
            'e_id' => 'nullable|integer'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response($validator->failed());
        }

        $product = Product::findOrfail($id);
        $product->title = $request->title;
        $product->price = $request->price;
        $product->e_id = isset($request->e_id) ? $request->e_id : null;
        $product->save();
 
        $templateData = "HI";
        // Mail::send('email.notification', $templateData, 
        //     function ($m) use ($templateData) {
        //     $m->from(getenv('MAIL_FROM_ADDRESS'), getenv('MAIL_FROM_NAME'));
        //     $m->to(getenv('MAIL_TO_ADDRESS'), 'Dear')->subject('add data');
        // });
        return response()->json(['message' => 'the product is successfully updated!'], 200);
    }

    public function delete($id) {
        $product = Product::findOrFail($id);
        $product->delete();

        return response()->json(['message' => 'the product is successfully deleted!'], 200);

    }
}
